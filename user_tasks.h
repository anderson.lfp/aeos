/**
 * SalimOS - Salim Operating System
 * by Sistemas Operacionais Embarcados - 2019/2 
 */

#ifndef USER_TASKS_H
#define	USER_TASKS_H

// Fun��o obrigat�ria
void user_tasks_config();

// Fun��es de usu�rios
void task_um();
void task_dois();
void task_tres();

#endif	/* USER_TASKS_H */

