/**
 * SalimOS - Salim Operating System
 * by Sistemas Operacionais Embarcados - 2019/2 
 */

#include <pic18f4520.h>

#include "user_tasks.h"
#include "kernel.h"
#include "sralloc.h"
#include "semaphore.h"
#include "pipe.h"
#include <xc.h>

byte *buffer;
t_sem teste, teste2, teste3;
t_pipe msg;

// Fun��o obrigat�ria
void user_tasks_config()  
{  
  // Configura��o das portas de comunica��o que as tarefas
  // de usu�rio ir�o manipular
  TRISDbits.RD0 = 0;
  TRISDbits.RD1 = 0;
  TRISDbits.RD2 = 0;
  // Idle
  TRISDbits.RD3 = 0;
  
  PORTDbits.RD0 = 0;
  PORTDbits.RD1 = 0;
  PORTDbits.RD2 = 0;
  // Idle
  PORTDbits.RD3 = 0;
  
  buffer = SRAMalloc(10);
  
  sem_create(&teste, 1);
  sem_create(&teste2, 0);
  sem_create(&teste3, 0);
  pipe_create(&msg);
}

// Fun��es de usu�rios
void task_um()
{
  //buffer[0] = 'a';
   while (1) {
     //sem_wait(&teste);
     //pipe_write(&msg, 'a');
     PORTDbits.RD0 = ~PORTDbits.RD0;
     //delay_os(300);
     //sem_post(&teste2);
  }
}

void task_dois()
{
  byte dado;
  
  while (1) {
    //sem_wait(&teste2);
    //pipe_read(&msg, &dado);
    //if (dado == 'a')
      //PORTDbits.RD1 = ~PORTDbits.RD1;    
    Nop();
    //delay_os(100);
    //sem_post(&teste3);
  }
}

void task_tres()
{
  while (1) {
   // sem_wait(&teste3);
    PORTDbits.RD2 = ~PORTDbits.RD2;
    //delay_os(100);
    //sem_post(&teste);
  }
}

